# -*- coding: utf-8 -*-
from __future__ import division, print_function
from dolfin import *

set_log_level(LogLevel.WARNING)
from field.testfield import TestFieldRauhut
from .parallel import ParallelizableProblem


class Problem(ParallelizableProblem):
    def __init__(self, info):
        assert info["problem"]["name"] == "darcy_rauhut"
        self.info = info

        # setup fe space
        self.space = FunctionSpace(self.mesh, "CG", self.degree)
        self.space_coef = FunctionSpace(self.mesh, "DG", self.degree - 1)

        # setup random field
        M = self.info["expansion"]["size"]
        decay = self.info["expansion"]["decay rate"]
        self.field = TestFieldRauhut(self.space_coef, M, decay)

        # define forcing term
        self.forcing = Constant(self.info["expansion"]["forcing"])

        # define boundary condition
        self.bc = DirichletBC(self.space, Constant(0.0), "on_boundary")

    @property
    def mesh(self):
        mesh = self.info["fe"]["mesh"]
        if isinstance(mesh, int):
            mesh = Mesh(UnitIntervalMesh(mesh))
            self.info["fe"]["mesh"] = mesh
        elif not isinstance(mesh, Mesh):
            mesh = Mesh(mesh)
            self.info["fe"]["mesh"] = mesh
        return mesh

    def coefficient(self, y):
        return self.field.realisation(y)

    def coefficient_vector(self, y):
        return self.coefficient(y).vector().get_local()

    def solution(self, y):
        """
        Return solution of Darcy problem for given parameter realization y.

        Parameter
        ---------
        y   :   array_like
                Sample for realization of the problem.

        Returns
        -------
        u   :   solution vector (numpy array)
        """
        V = self.space
        f = self.forcing
        bc = self.bc
        kappa = self.coefficient(y)

        u = TrialFunction(V)
        v = TestFunction(V)
        a = kappa * inner(grad(u), grad(v)) * dx
        L = f * v * dx

        u = Function(V)
        solve(a == L, u, bc)

        return u.vector().get_local()
